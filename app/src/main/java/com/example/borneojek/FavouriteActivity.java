package com.example.borneojek;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.borneojek.adapter.AdapterFavourite;
import com.example.borneojek.helper.SingletonRequest;
import com.example.borneojek.interfaces.StandardCallback;
import com.example.borneojek.model.ModelMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FavouriteActivity extends AppCompatActivity {

    RecyclerView mList;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mManager;

    List<ModelMenu> mListItem;

    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.favourite_activities);

        ImageButton btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mList = findViewById(R.id.list);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);

        mListItem = new ArrayList<ModelMenu>();

        mManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mList.setLayoutManager(mManager);
        mAdapter = new AdapterFavourite(mListItem, this);
        mList.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                request(new StandardCallback() {
                    @Override
                    public void onSuccess() {
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        request();
    }

    private void request() {
        request(new StandardCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });
    }

    private void request(StandardCallback callback) {
        mListItem.clear();
        JsonArrayRequest rq = new JsonArrayRequest(Request.Method.GET, ScrollingActivity.BASE_URL + "/menu/getFavouriteOnly", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jObj = response.getJSONObject(i);
                        ModelMenu obj = new ModelMenu(
                                Integer.parseInt(jObj.getString("id")),
                                jObj.getString("title"),
                                jObj.getString("description"),
                                jObj.getString("image_url"),
                                Double.parseDouble(jObj.getString("final_price")),
                                Double.parseDouble(jObj.getString("original_price")),
                                0,
                                jObj.getString("is_favourite").equals("1")
                        );

                        mListItem.add(obj);
                        callback.onSuccess();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError();
            }
        });
        SingletonRequest.getInstance(this).addToRequestQueue(rq);
    }
}