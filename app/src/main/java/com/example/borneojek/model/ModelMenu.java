package com.example.borneojek.model;

public class ModelMenu {
    String title, desc, imageURL;
    double finalPrice, originalPrice;
    int count, id;
    boolean isFavourite;

    public ModelMenu(int id, String title, String desc, String imageURL, double finalPrice, double originalPrice, int count, boolean isFavourite) {
        this.title = title;
        this.desc = desc;
        this.imageURL = imageURL;
        this.finalPrice = finalPrice;
        this.originalPrice = originalPrice;
        this.count = count;
        this.isFavourite = isFavourite;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public double getAmount() {
        return count * finalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
