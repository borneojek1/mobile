package com.example.borneojek.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.borneojek.FavouriteActivity;
import com.example.borneojek.R;
import com.example.borneojek.ScrollingActivity;
import com.example.borneojek.helper.SingletonRequest;
import com.example.borneojek.model.ModelMenu;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class AdapterFavourite extends RecyclerView.Adapter<HolderFavourite> {

    List<ModelMenu> mListItem;
    Context context;

    public AdapterFavourite(List<ModelMenu> mListItem, Context context) {
        this.mListItem = mListItem;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderFavourite onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_menu, parent, false);
        return new HolderFavourite(layout, this);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderFavourite holder, int position) {
        ModelMenu mItem = mListItem.get(position);
        holder.mnTitle.setText(mItem.getTitle());
        holder.mnDesc.setText(mItem.getDesc());
        double originalPrice = mItem.getOriginalPrice(), finalPrice = mItem.getFinalPrice();

        holder.mnFinalPrice.setText(String.format("Rp%,.0f", finalPrice));
        if (originalPrice < finalPrice) {
            holder.mnOriginalPrice.setPaintFlags(holder.mnOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.mnOriginalPrice.setVisibility(View.VISIBLE);
            holder.iconPrice.setVisibility(View.VISIBLE);
            holder.mnOriginalPrice.setText(String.format("Rp%,.0f", originalPrice));
        } else {
            holder.mnOriginalPrice.setVisibility(View.GONE);
            holder.iconPrice.setVisibility(View.GONE);
        }

        if (mItem.isFavourite()) {
            holder.mnLove.setImageResource(R.drawable.ic_love_on);
        } else {
            holder.mnLove.setImageResource(R.drawable.ic_love_off);
        }

        if (mItem.getCount() > 0) {
            holder.mnAdd.setVisibility(View.GONE);
            holder.mnParentPlusMin.setVisibility(View.VISIBLE);
        } else {
            holder.mnParentPlusMin.setVisibility(View.GONE);
            holder.mnAdd.setVisibility(View.VISIBLE);
        }

        holder.mnCount.setText(String.format("%d", mItem.getCount()));
        Glide.with(context).load(mItem.getImageURL())
                .transition(DrawableTransitionOptions.withCrossFade())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(holder.mnImage);


        int totalItem = 0;
        double totalAmount = 0;
        for (ModelMenu i : mListItem) {
            totalItem += i.getCount();
            totalAmount += i.getAmount();
        }

        CardView vw = ((Activity) context).findViewById(R.id.parent_checkout_fav);
        if (totalItem > 0) {
            vw.setVisibility(View.VISIBLE);
            TextView tTotalCount = ((Activity) context).findViewById(R.id.txt_btc_total_count);
            TextView tTotalAmount = ((Activity) context).findViewById(R.id.txt_btc_total_amount);
            tTotalCount.setText(String.format("%d Item", totalItem));
            tTotalAmount.setText(String.format("Rp%,.0f", totalAmount));
        } else {
            vw.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mListItem.size();
    }
}

class HolderFavourite extends RecyclerView.ViewHolder {
    ImageView iconPrice;
    ShapeableImageView mnImage;
    TextView mnTitle, mnDesc, mnFinalPrice, mnOriginalPrice, mnCount;
    ImageButton mnLove;
    LinearLayout mnParentPlusMin;
    Button mnAdd;

    private AdapterFavourite adapter;

    public HolderFavourite(View itemView, AdapterFavourite adapter) {
        super(itemView);

        this.iconPrice = (ImageView) itemView.findViewById(R.id.mn_iconPrice);
        this.mnImage = (ShapeableImageView) itemView.findViewById(R.id.mn_image);
        this.mnTitle = (TextView) itemView.findViewById(R.id.mn_title);
        this.mnDesc = (TextView) itemView.findViewById(R.id.mn_desc);
        this.mnFinalPrice = (TextView) itemView.findViewById(R.id.mn_finalPrice);
        this.mnOriginalPrice = (TextView) itemView.findViewById(R.id.mn_originalPrice);
        this.mnLove = (ImageButton) itemView.findViewById(R.id.mn_love);
        this.mnParentPlusMin = (LinearLayout) itemView.findViewById(R.id.mn_parentPlusMin);
        this.mnAdd = (Button) itemView.findViewById(R.id.mn_add);
        this.mnCount = (TextView) itemView.findViewById(R.id.mn_count);

        itemView.findViewById(R.id.mn_add).setOnClickListener(v -> {
            ModelMenu item = adapter.mListItem.get(getAdapterPosition());
            item.setCount(item.getCount() + 1);
            adapter.mListItem.set(getAdapterPosition(), item);
            adapter.notifyItemChanged(getAdapterPosition());

        });
        itemView.findViewById(R.id.mn_plus).setOnClickListener(v -> {
            ModelMenu item = adapter.mListItem.get(getAdapterPosition());
            item.setCount(item.getCount() + 1);
            adapter.mListItem.set(getAdapterPosition(), item);
            adapter.notifyItemChanged(getAdapterPosition());
        });
        itemView.findViewById(R.id.mn_min).setOnClickListener(v -> {
            ModelMenu item = adapter.mListItem.get(getAdapterPosition());
            item.setCount(item.getCount() - 1);
            adapter.mListItem.set(getAdapterPosition(), item);
            adapter.notifyItemChanged(getAdapterPosition());
        });
        itemView.findViewById(R.id.mn_love).setOnClickListener(v -> {
            doFavourite(itemView);
        });

        this.adapter = adapter;
    }

    private void doFavourite(View itemView) {
        ModelMenu item = adapter.mListItem.get(getAdapterPosition());
        JsonObjectRequest rq = new JsonObjectRequest(Request.Method.GET, ScrollingActivity.BASE_URL + "/menu/favourite?id=" + item.getId(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        boolean setTo = !item.isFavourite();
                        item.setFavourite(setTo);
                        adapter.mListItem.set(getAdapterPosition(), item);
                        adapter.notifyItemChanged(getAdapterPosition());
                        int msg = setTo ? R.string.msg_successfully_favourite : R.string.msg_successfully_not_favourite;
                        Snackbar.make(itemView, msg, Snackbar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .setAction(R.string.txt_see, it -> {
                                    Log.d("SUCCESS", "Success");

                                    Intent favActivityIntent = new Intent(itemView.getContext(), FavouriteActivity.class);
                                    itemView.getContext().startActivity(favActivityIntent);

//                                        Toast.makeText(itemView, "tes", Toast.LENGTH_SHORT).show();
                                }).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        SingletonRequest.getInstance(itemView.getContext()).addToRequestQueue(rq);
    }
}
