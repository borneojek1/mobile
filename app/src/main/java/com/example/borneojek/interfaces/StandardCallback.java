package com.example.borneojek.interfaces;

public interface StandardCallback {
    void onSuccess();
    void onError();
}
