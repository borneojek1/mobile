package com.example.borneojek.helper;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class SingletonRequest {
    private static SingletonRequest instance;
    private RequestQueue queue;
    private static Context context;

    private SingletonRequest(Context ctx) {
        context = ctx;
        queue = getRequestQueue();
    }

    public static synchronized SingletonRequest getInstance(Context ctx) {
        if (instance == null) {
            instance = new SingletonRequest(ctx);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (queue == null) {
            queue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return queue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
