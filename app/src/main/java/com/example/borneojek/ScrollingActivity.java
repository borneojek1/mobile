package com.example.borneojek;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.borneojek.adapter.AdapterMenu;
import com.example.borneojek.helper.SingletonRequest;
import com.example.borneojek.interfaces.StandardCallback;
import com.example.borneojek.model.ModelMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ScrollingActivity extends AppCompatActivity {

    RecyclerView mList;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mManager;

    List<ModelMenu> mListItem;

    SwipeRefreshLayout swipeRefreshLayout;

    public final static String BASE_URL = "https://test123fzn.000webhostapp.com/api";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.activity_scrolling);

        mList = findViewById(R.id.list);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);

//        String desc = "Mie dengan rasa asin dan pedas, dengan ditaburi daging ayam cincang beserta pangsit yang kriuknya bikin nagih";
//        String imgUri = "https://petualang.travelingyuk.com/unggah/2019/07/lrm_export_801357431073155_20190725_113547477_sMd.jpg";
//        mListItem = new ArrayList<ModelMenu>(Arrays.asList(new ModelMenu[]{
//                new ModelMenu("Title 1", desc, imgUri, 20000, 19000, 0, false),
//                new ModelMenu("Title 1", desc, imgUri, 20000, 19000, 0, false),
//                new ModelMenu("Title 1", desc, imgUri, 20000, 19000, 0, false),
//                new ModelMenu("Title 1", desc, imgUri, 20000, 19000, 0, false),
//                new ModelMenu("Title 1", desc, imgUri, 20000, 19000, 0, false),
//                new ModelMenu("Title 1", desc, imgUri, 20000, 19000, 0, false),
//                new ModelMenu("Title 1", desc, imgUri, 20000, 19000, 0, false),
//        }));

        mListItem = new ArrayList<ModelMenu>();

        mManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mList.setLayoutManager(mManager);
        mAdapter = new AdapterMenu(mListItem, this);
        mList.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                request(new StandardCallback() {
                    @Override
                    public void onSuccess() {
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        request();
    }

    private void request() {
        request(new StandardCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });
    }

    private void request(StandardCallback callback) {
        mListItem.clear();
        JsonArrayRequest rq = new JsonArrayRequest(Request.Method.GET, BASE_URL + "/menu", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jObj = response.getJSONObject(i);
                        ModelMenu obj = new ModelMenu(
                                Integer.parseInt(jObj.getString("id")),
                                jObj.getString("title"),
                                jObj.getString("description"),
                                jObj.getString("image_url"),
                                Double.parseDouble(jObj.getString("final_price")),
                                Double.parseDouble(jObj.getString("original_price")),
                                0,
                                jObj.getString("is_favourite").equals("1")
                        );

                        mListItem.add(obj);
                        callback.onSuccess();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError();
            }
        });
        SingletonRequest.getInstance(this).addToRequestQueue(rq);
    }
}